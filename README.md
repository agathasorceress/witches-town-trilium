# Witches Town's theme for Trilium Notes

A port of [Witches Town](https://github.com/wryk/witches-town-theme) theme for Trilium Notes with a few additional changes.

![Screenshot of Trilium Notes with theme enabled](https://i.imgur.com/2wNyvbe.png)

# Installation

Compress the files into a zip archive and import into the app.

# Licence

Copyright (C) 2017-2018 Alda Marteau-Hardi & other contributors.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/.
